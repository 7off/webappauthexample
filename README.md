Пример приложения с авторизацией и использованием IdentityServer'а в качестве ExternalProvider'а.
В шаблонное приложение добавил настройку OAuth (использование AddOAuth Extension'а в Startup'е).
+ обработку callback-запроса ("/authorization-code/callback") и получение токена пользователя.

Настроил на тестовый IdentityServer bv5.

> Возможно есть более простой способ с исползованием уже готовых Extension'ов. Можно поискать например в IdentityModel.AspNetCore.OAuth2Introspection, IdentityServer4.AspNetIdentity или ещё каких-нибудь библиотеках. Я попытался исползовать AddOpenIdConnect, но быстро не получилось