using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using WebApplicationAuth2.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using System.Net.Http.Headers;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Text.Json;
using System.IO;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;

namespace WebApplicationAuth2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();

            //services.AddAuthentication().AddOpenIdConnect("IntraDeskExternal", "IntraDesk", options => {               

            //    options.SignInScheme = "Cookies";

            //    options.Authority = Configuration["AuthorizationOptions:IdentityServerUrl"];
            //    options.RequireHttpsMetadata = false;

            //    options.ClientId = Configuration["AuthorizationOptions:ClientId"];
            //    options.ClientSecret = Configuration["AuthorizationOptions:ClientSecret"];
            //    options.SaveTokens = true;
            //});


            // ���������� IdentityServer'� � �������� ExternalProvider
            services.AddAuthentication().AddOAuth("IntraDeskExternal", "IntraDesk", options =>
            {
                options.AuthorizationEndpoint = $"{Configuration["AuthorizationOptions:IdentityServerUrl"]}/connect/authorize";
                options.TokenEndpoint = $"{Configuration["AuthorizationOptions:IdentityServerUrl"]}/connect/token";
                options.UserInformationEndpoint = $"{Configuration["AuthorizationOptions:IdentityServerUrl"]}/connect/userinfo";

                options.ClientId = Configuration["AuthorizationOptions:ClientId"];
                options.ClientSecret = Configuration["AuthorizationOptions:ClientSecret"];

                options.CallbackPath = new PathString("/authorization-code/callback");

                options.Scope.Add("openid");
                options.Scope.Add("profile");
                options.Scope.Add("email");
                options.Scope.Add("custom.profile");
                options.Scope.Add("api");

                options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "sub");

                options.Events = new OAuthEvents
                {
                    OnRedirectToAuthorizationEndpoint = async context =>
                    {
                        context.HttpContext.Response.Redirect(context.RedirectUri + $"&acr_values=tenant:{Configuration["AuthorizationOptions:Tenant"]}");
                    }
                };
            });

            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.Map("/authorization-code/callback", appBuilder =>
            {
                appBuilder.Run(async (context) =>
                {
                    var queryData = context.Request.Query;
                    var code = queryData.ContainsKey("code") ? queryData["code"].ToString() : null;
                    //var scope = queryData.ContainsKey("scope") ? queryData["scope"].ToString() : null;
                    //var state = queryData.ContainsKey("state") ? queryData["state"].ToString() : null;
                    //var sessionstate = queryData.ContainsKey("session_state") ? queryData["session_state"].ToString() : null;

                    //await context.Response.WriteAsync($"code: {code}");

                    var httpClient = new HttpClient();
                    var tokenEndpoint = $"{Configuration["AuthorizationOptions:IdentityServerUrl"]}/connect/token";
                    var userInformationEndpoint = $"{Configuration["AuthorizationOptions:IdentityServerUrl"]}/connect/userinfo";

                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("grant_type", "authorization_code"),

                        // redirect_uri = url ���.���������� + callback urn
                        new KeyValuePair<string, string>("redirect_uri", "https://localhost:44340/authorization-code/callback"),
                        new KeyValuePair<string, string>("code", code),
                        new KeyValuePair<string, string>("client_id", Configuration["AuthorizationOptions:ClientId"]),
                        new KeyValuePair<string, string>("client_secret", Configuration["AuthorizationOptions:ClientSecret"])
                    });

                    var tokenResponse = await httpClient.PostAsync(tokenEndpoint, formContent);
                    string text = await tokenResponse.Content.ReadAsStringAsync();

                    if (!tokenResponse.IsSuccessStatusCode)
                    {
                        //todo ���������� ������ �����������
                    }

                    var jsonResponse = JsonConvert.DeserializeObject<dynamic>(text);

                    string idTokenRaw = jsonResponse["id_token"];
                    string accessTokenRaw = jsonResponse["access_token"];
                    string expiresRaw = jsonResponse["expires_in"];

                    var handler = new JwtSecurityTokenHandler();
                    var idToken = !string.IsNullOrEmpty(idTokenRaw) ? handler.ReadToken(idTokenRaw) as JwtSecurityToken : new JwtSecurityToken();
                    var accessToken = !string.IsNullOrEmpty(accessTokenRaw) ? handler.ReadToken(accessTokenRaw) as JwtSecurityToken : new JwtSecurityToken();

                    var idClaim = accessToken.Claims.FirstOrDefault(x => x.Type == "sub");

                    //todo ���� ������������ � ����������

                    await context.Response.WriteAsync($"id-token: {idToken}");
                });
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
